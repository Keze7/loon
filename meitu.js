/*******************************
应用名称：美图秀秀
脚本功能：解锁会员
应用版本：9.6.20
应用下载：App Store
脚本作者：Kaze
更新时间：2023-06-28
脚本发布：暂无
使用声明：‼️仅供学习交流, 🈲️商业用途
*******************************
[Script]
http-requesthttps?:\/\/api\.xiuxiu\.meitu\.com\/v1\/user\/*$ script-path=https://raw.githubusercontent.com/WeiRen0/Scripts/main/meitu.js, timeout=60 ,tag=meitu
[MITM]
hostname = api.xiuxiu.meitu.com
*******************************/
let obj = JSON.parse($response.body);
   
       obj = {
           "avatar_url": "https://maavatar1.meitudata.com/5a91a19a672653d1c9432d687da800b1.jpg",
               "feed_count": 0,
                   "identity_type": 0,
                       "video_template_feed_count": 0,
                           "background_url": "https://xximg1.meitudata.com/6531090531059762177.png",
                               "last_update_time": 0,
                                   "vip_type": 1,
                                       "identity_card": "",
                                           "template_feed_count": 0,
                                               "is_live": 0,
                                                   "portal_icon": "",
                                                       "card_item": [],
                                                           "is_authorize": 1,
                                                               "mt_chat": false,
                                                                   "has_shop_permission": 0,
                                                                       "city_id": 0,
                                                                           "show_producer_level": 999,
                                                                               "is_expert_user": 1,
                                                                                   "topic_favorite_count": 0,
                                                                                       "country_id": 0,
                                                                                           "type": 0,
                                                                                               "desc": "",
                                                                                                   "feed_like_count": 0,
                                                                                                       "follower_count": 4,
                                                                                                           "identity_desc": "",
                                                                                                               "identity_new_status": 0,
                                                                                                                   "be_favorites_count": 0,
                                                                                                                       "portal_url": "",
                                                                                                                           "create_time": 1669732299,
                                                                                                                               "screen_name": "留胡子的鸭子🛃🦛",
                                                                                                                                   "is_invited": 0,
                                                                                                                                       "vip_icon": "https://xximg1.meitudata.com/6948531818264286440.png",
                                                                                                                                           "is_preset": 0,
                                                                                                                                               "identity_schema": "https://titan-h5.meitu.com/xiu-h5/authcard/index.html",
                                                                                                                                                   "portal_type": 0,
                                                                                                                                                       "pendants_content": [],
                                                                                                                                                           "favorites_count": 0,
                                                                                                                                                               "core": true,
                                                                                                                                                                   "has_permission": 1,
                                                                                                                                                                       "landmark_count": "0",
                                                                                                                                                                           "age": 9999,
                                                                                                                                                                               "pendants": [],
                                                                                                                                                                                   "birthday": 631123200,
                                                                                                                                                                                       "mt_num": 0,
                                                                                                                                                                                           "identity_time": 0,
                                                                                                                                                                                               "scheme": "",
                                                                                                                                                                                                   "uid": 1932334474,
                                                                                                                                                                                                       "level": 99,
                                                                                                                                                                                                           "friendship_status": 0,
                                                                                                                                                                                                               "identity_status": 0,
                                                                                                                                                                                                                   "in_blacklist": 0,
                                                                                                                                                                                                                       "gender": "f",
                                                                                                                                                                                                                           "be_like_count": 0,
                                                                                                                                                                                                                               "show_shopping_cart": 0,
                                                                                                                                                                                                                                   "free_trial": 1,
                                                                                                                                                                                                                                       "have_unlock_landmark": 0,
                                                                                                                                                                                                                                           "fan_count": 2,
                                                                                                                                                                                                                                               "landmark_ranking": "0",
                                                                                                                                                                                                                                                   "constellation": "Capricorn",
                                                                                                                                                                                                                                                       "video_favorites_count": 0,
                                                                                                                                                                                                                                                           "feed_favorites_count": 0,
                                                                                                                                                                                                                                                               "magazine_count": 0,
                                                                                                                                                                                                                                                                   "province_id": 0
       }

       $done({body : JSON.stringify(obj)});
       }
